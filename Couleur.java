/**
 * Couleur est une classe repr�sentant une couleur au moyen d'un code RVB
 * @author Jacques
 *
 */
public class Couleur {
	/**
	 * Valeur rouge cr�e dans le constructeur. Elle est accessible � la lecture et modifiable
	 * 
	 * @see Couleur#getRouge()
	 * @see Couleur#setRouge(int)
	 */
	private int rouge;
	
	/**
	 * Valeur bleu cr�e dans le constructeur. Elle est accessible � la lecture et modifiable
	 * 
	 * @see Couleur#getBleu()
	 * @see Couleur#setBleu(int)
	 */
	private int vert;
	
	/**
	 * Valeur bleu cr�e dans le constructeur. Elle est accessible � la lecture et modifiable
	 * 
	 * @see Couleur#getBleu()
	 * @see Couleur#setBleu(int)
	 */
	private int bleu;

	/**
	 * Constante de class Rouge
	 */
	public static final Couleur ROUGE = new Couleur (255, 0, 0);
	/**
	 * Constante de class Vert
	 */
	public static final Couleur VERT = new Couleur (0, 255, 0);
	/**
	 * Constante de class Bleu
	 */
	public static final Couleur BLEU = new Couleur (0, 0, 255);
	
	/**
	 * Constructeur de la class Couleur
	 * A la construction d'un objet Couleur sont initi� les valeurs rouge, vert et bleu
	 * @param rouge
	 * 			La valeur rouge
	 * @param vert
	 * 			La valeur verte
	 * @param bleu
	 * 			La valeur bleu
	 * @throws IllegalArgumentException
	 */
	public Couleur (int rouge, int vert, int bleu) throws IllegalArgumentException {
		this.setRouge(rouge);
		this.setVert(vert);
		this.setBleu(bleu);
	}
	
	/**
	 * Retourne la valeur Rouge
	 * 
	 * @return la valeur rouge sous la forme d'un int
	 */
	public int getRouge() {
		return (this.rouge);
	}
	
	/**
	 * Retourne la valeur Verte
	 * 
	 * @return la valeur verte sous la forme d'un int
	 */
	public int getVert() {
		return (this.vert);
	}
	
	/**
	 * Retourne la valeur Bleu
	 * 
	 * @return la valeur bleu sous la forme d'un int
	 */
	public int getBleu() {
		return (this.bleu);
	}
	
	/**
	 * Retourne la valeur RVB
	 * 
	 * @return la valeur RVB sous la forme d'un int
	 */
	public int valeurRVB() {
		return this.rouge * 256 * 256 + this.vert * 256 + this.bleu;
	}
	
	/**
	 * modifie la valeur rouge
	 * 
	 * @param rouge
	 * 			La valeur du rouge
	 * @throws IllegalArgumentException
	 * 			Erreur si la valeur n'est pas comprise entre 0 et 255
	 */
	public void setRouge(int rouge) throws IllegalArgumentException {
		if (rouge < 0 || rouge > 255)
			throw new IllegalArgumentException ("mauvaise nuance de rouge " + rouge);
		this.rouge = rouge;
	}
	
	/**
	 * modifie la valeur verte
	 * 
	 * @param vert
	 * 			La valeur du vert
	 * @throws IllegalArgumentException
	 * 			Erreur si la valeur n'est pas comprise entre 0 et 255
	 */
	public void setVert(int vert) throws IllegalArgumentException {
		if (vert < 0 || vert > 255)
			throw new IllegalArgumentException ("mauvaise nuance de vert " + vert);
		this.vert = vert;
	}
	
	/**
	 * modifie la valeur bleu
	 * 
	 * @param bleu
	 * 			La valeur du bleu
	 * @throws IllegalArgumentException
	 * 			Erreur si la valeur n'est pas comprise entre 0 et 255
	 */
	public void setBleu(int bleu) throws IllegalArgumentException {
		if (bleu < 0 || bleu > 255)
			throw new IllegalArgumentException ("mauvaise nuance de bleu " + bleu);
		this.bleu = bleu;
	}
	
	/**
	 * Retourne la chaine de caractere representant RVB
	 * @return Retourne la chaine de caractere representant le code RVB
	 */
	@Override
	public String toString() {
		return "[" + rouge + "," + vert + "," + bleu + "]";
	}	
}
