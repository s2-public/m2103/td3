
public class Segment {

	private Point origine;
	private Point extremite;
	
	public Segment (Point origine, Point extremite) {
		this.origine= origine;
		this.extremite=extremite;
	}
	
	public Point pointMillieu() {
		return new Point((this.origine.getAbscisse()+this.extremite.getAbscisse())/2,
				(this.origine.getOrdonnee()+this.extremite.getOrdonnee())/2);
	}
	
}
